class Hangman
  attr_accessor :guesser, :referee, :board
  $maximum_guesses = 5

  def initialize (players = { :guesser => player1, :referee => player2 })
    @guesser = players[:guesser]
    @referee = players[:referee]
    @remaining_guesses = $maximum_guesses
  end


  def setup
    #gets word_length from referee
    length_of_secret_word = @referee.pick_secret_word

    #gives the word_length to the guesser
    @guesser.register_secret_length(length_of_secret_word)

    #creates a blank board based on length of word
    @board = String.new
    length_of_secret_word.times { @board += "-" }
  end


  def take_turn
    guessed_letter = guesser.guess(@board)
    indices = referee.check_guess(guessed_letter)
    update_board(indices, guessed_letter)

    @remaining_guesses -= 1 if indicies.empty?

    display_board

    @guesser.handle_response(guessed_letter)
  end


  def update_board(indices, guessed_letter)
    indices.each { |idx| @board[idx] = guessed_letter }
    @board
  end


  def display_board
    p @board
  end


def play
  setup do
  while @remaining_guesses > 0
    take_turn

    if won?
      puts "You win!"
      return
    end

  puts "Wrong guess."
  end
end


  end
end



class HumanPlayer

  attr_accessor :dictionary, :secret_word, :length_of_secret_word

  def initialize(dictionary)
    if dictionary.class == Array
      @dictionary = dictionary
    else
      dictionary_file = open("lib/#{dictionary}", "r")
      @dictionary = File.readlines(dictionary_file).map! { |line| line.chomp }
    end
  end


  def register_secret_length(length_of_secret_word)
    puts "Length of secret word is #{length_of_secret_word}."
  end

  def guess(board)
    puts "Please guess a letter"
    $stdin.gets.chomp.downcase
  end

  def pick_secret_word
    puts "Type the secret word."
    @secret_word = $stdin.gets.chomp.downcase
    @length_of_secret_word = @secret_word.length
  end

  def check_guess(letter_string)
    array = Array.new
    @secret_word.chars.each_with_index do |char, index|
      array << index if char == letter_string
    end
    array
  end



  def handle_response(guessed_letter)
    if referee.secret_word.include?(letter)
      puts "You guessed correctly!"
    else
      puts "You guessed incorrectly."
    end
  end

  #should contain the logic both for (1) guessing letters and (2) confirming guesses.
  #Make your HumanPlayer and ComputerPlayer classes interchangeable


end

class ComputerPlayer
  #computer chooses secret word (from dictionary) and player tries to guess it
  #should contain the logic both for (1) guessing letters and (2) confirming guesses.
  #Make your HumanPlayer and ComputerPlayer classes interchangeable

  attr_accessor :dictionary, :secret_word, :length_of_secret_word

  def initialize(dictionary)
    if dictionary.class == Array
      @dictionary = dictionary
    else
      dictionary_file = open("lib/#{dictionary}", "r")
      @dictionary = File.readlines(dictionary_file).map! { |line| line.chomp }
    end
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @length_of_secret_word = @secret_word.length
  end

  def check_guess(letter_string)
    array = Array.new
    @secret_word.chars.each_with_index do |char, index|
      array << index if char == letter_string
    end
    array
  end


end


# def read_me(dictionary)
#   puts dictionary
#   dictionary_file = open("lib/#{dictionary}", "r")
#   dict = File.readlines(dictionary_file).map! { |line| line.chomp }
#   print dict
# end
#
# read_me("dictionary.txt")
